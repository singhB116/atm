import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Scanner;
public class ATM {
    private static int idChoice = 0;
    private static int index = 0;
    private static int chosenCust = 0;
    private static int choice = 0;
    public static String chosen = "";


    public static ArrayList<BankAcct> accountList = new ArrayList<BankAcct>();
    public static void populate() {
        accountList.add(new BankAcct("Brandon Singh", 1234, 5218, 0.0, 0.0));
        accountList.add(new BankAcct("Leann Mahadeo", 4321, 8125, 0.0, 0.0));
        accountList.add(new BankAcct("Kyle Minoza", 9876, 6644, 0.0, 0.0));
        accountList.add(new BankAcct("Ryan Francis", 6789, 4466, 0.0, 0.0));
    }

    public static void init() {
        //Scanner kb = new Scanner(System.in);
        GUI gui = new GUI("St. John's University Bank", "Enter customer number: ");
        gui.setVisible(true);
        gui.aButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                idChoice = Integer.parseInt(gui.textField1.getText());
                find(accountList, idChoice);
            }
        });
        System.out.println("Enter customer number: ");
        //idChoice = kb.nextInt();
        //find(accountList, idChoice);
    }

    public static void find(ArrayList<BankAcct> list, int choice) {
        choice = idChoice;
        String looking = "Looking for: " + choice;
        GUI gui = new GUI("St. John's University Bank", looking);
        gui.setVisible(true);
        System.out.println("Looking for: " + choice);
        int count = 0;
        int[] ids = new int[list.size()];
        while(count < list.size()) {
            ids[count] = (list.get(count).getId(list.get(count)));
            count++;
        }
        for(int i=0; i<ids.length; i++) {
            if(choice == ids[i]) {
                index = i;
            }
        }
        authenticate(accountList.get(index));
    }

    public static void authenticate(BankAcct customer) {
        Scanner kb = new Scanner(System.in);
        GUI gui = new GUI("St. John's University Bank", "Enter your pin number: ");
        gui.setVisible(true);
        System.out.println("Enter your pin number: ");
        gui.aButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int pin = Integer.parseInt(gui.textField1.getText());
                if(pin == customer.getPin(customer)) {
                    chosenCust = customer.getId(customer);
                }
                else {
                    GUI gui = new GUI("St. John's University Bank", "Wrong pin.");
                    gui.setVisible(true);
                    init();
                }
                checkorsave(customer);
            }
        });
        if(kb.nextInt() == customer.getPin(customer)) {
            chosenCust = customer.getId(customer);
        }
        else {
            System.out.println("Wrong pin.");
            init();
        }
        checkorsave(customer);
    }

    public static void checkorsave(BankAcct customer) {
        Scanner kb = new Scanner(System.in);
        GUI gui = new GUI("St. John's University Bank", "A for Checkings, B for Savings, Or C to cancel: ");
        gui.setVisible(true);
        System.out.println("A for Checkings, B for Savings, Or C to cancel: ");
        String choose = kb.nextLine();
        gui.aButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double disp = BankAcct.getCheckingBal(customer);
                String disp2 = String.valueOf(disp);
                GUI gui = new GUI("St. John's University Bank", disp2);
            }
        });
        gui.bButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double disp = BankAcct.getSavingsBal(customer);
                String disp2 = String.valueOf(disp);
                GUI gui = new GUI("St. John's University Bank", disp2);
            }
        });
        gui.bButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                init();
            }
        });
        chosen = choose;
        switch(choose) {
            case "A" :
                System.out.println(BankAcct.getCheckingBal(customer));
                break;
            case "B" :
                System.out.println(BankAcct.getSavingsBal(customer));
                break;
            case "C" :
                init();
            default:
                System.out.println("Enter another letter.");
        }
        withordep(customer);
    }

    public static void withordep(BankAcct customer) {
        Scanner kb = new Scanner(System.in);
        GUI gui = new GUI("St. John's University Bank", "A for Withdrawal, B for Deposit, Or C to cancel: ");
        gui.setVisible(true);
        System.out.println("A for Withdrawal, B for Deposit, Or C to cancel: ");
        String choose = kb.nextLine();
        gui.aButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GUI gui = new GUI("St. John's University Bank", "Enter the amount you would like to withdraw: ");
                double amount = Double.parseDouble(gui.textField1.getText());
                BankAcct.Withdraw(customer, amount, chosen);
                init();
            }
        });
        gui.bButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GUI gui = new GUI("St. John's University Bank", "Enter the amount you would like to deposit: ");
                double amount = Double.parseDouble(gui.textField1.getText());
                BankAcct.Deposit(customer, amount, chosen);
                init();
            }
        });
        gui.cButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                init();
            }
        });
        switch(choose) {
            case "A" :
                System.out.println("Enter the amount you would like to withdraw: ");
                BankAcct.Withdraw(customer, kb.nextDouble(), chosen);
                init();
                break;
            case "B" :
                System.out.println("Enter the amount you would like to deposit: ");
                BankAcct.Deposit(customer,kb.nextDouble(),chosen);
                init();
                break;
            case "C" :
                init();
            default:
                System.out.println("Enter another letter.");
        }
    }


    public static void main(String[] args) {
        populate();
        init();
    }
}
