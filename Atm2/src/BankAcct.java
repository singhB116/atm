import java.util.Scanner;
import java.util.ArrayList;

public class BankAcct {
    private String Name;
    private int customerNum;
    private int pin;
    private static double checkingBal = 0.0;
    private static double savingsBal = 0.0;

    public BankAcct(String n, int cNum, int p, double cBal, double sBal) {
        Name=n;
        customerNum=cNum;
        pin=p;
        checkingBal=cBal;
        savingsBal=sBal;
    }

    public static void populate() {
        ArrayList<BankAcct> accountList = new ArrayList<BankAcct>();
        accountList.add(new BankAcct("Brandon Singh", 1234, 5218, 0.0, 0.0));
        accountList.add(new BankAcct("Leann Mahadeo", 4321, 8125, 0.0, 0.0));
        accountList.add(new BankAcct("Kyle Minoza", 9876, 6644, 0.0, 0.0));
        accountList.add(new BankAcct("Ryan Francis", 6789, 4466, 0.0, 0.0));
    }


    public int getPin(BankAcct customer) {
        return customer.pin;
    }
    public int getId(BankAcct customer) {
        return customer.customerNum;
    }

    public static double getCheckingBal(BankAcct customer) {
        return checkingBal;
    }

    public static double getSavingsBal(BankAcct customer) {
        return savingsBal;
    }

    public void setsavingsBal(BankAcct customer, double amount) {
        customer.savingsBal = amount;
    }
    public void setcheckingsBal(BankAcct customer, double amount) {
        customer.checkingBal = amount;
    }
    public void showInfo() {
        System.out.println("Account information: " + "Name: " + Name + "Checking Balance: " + checkingBal + "Savings Balance: " + savingsBal);
    }
    public static double Deposit(BankAcct customer, double amount, String choice) {
        if(choice.equals("A")){
            customer.checkingBal+=amount;
            return customer.checkingBal;
        }
        else {
            customer.savingsBal+=amount;
            return customer.savingsBal;
        }
    }
    public static Double Withdraw(BankAcct customer, double amount, String choice) {
        if(choice.equals("A")) {
            if(amount>checkingBal){
                System.out.println("Insufficient funds.");
            }
            else {
                customer.checkingBal-=amount;
                return customer.checkingBal;
            }
        }
        if(choice.equals("B")) {
            if(amount>savingsBal) {
                System.out.println("Insufficient funds.");
            }
            else {
                customer.savingsBal-=amount;
                return customer.savingsBal;
            }
        }
        return null;
    }
}